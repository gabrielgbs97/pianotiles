package com.example.pedro.arbosbarcelo_exercici1;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.AsyncTask;

public class NotePlayer {
    static final int SAMPLE_RATE = 48000;
    int bitDepth = 16;
    int buffSize = SAMPLE_RATE*bitDepth/8;
    byte[] generatedSnd = {0,0};
    AudioTrack audioHandler;
    boolean fixed;

    public NotePlayer(boolean fixed){
        this.fixed = fixed;

    }

    /*This function will generate 1 second of samples at given sample rate and a bit depth of 16b.
     * Then will store them in generatedSnd array following BIG endian rule to store each pair of
     * bytes.
     * */
    public void setFullSignal(double frequency){
        generatedSnd = new byte[buffSize];
        double currentSoundDbl=0;
        short currentSound;
        int j = 0;
        double fadeMax = (double)buffSize/2;
        double fader=1;
        double faderStep=1/(fadeMax/2);

        for (int i = 0; i<buffSize/2; i++){
            //Current sample, amplified at its maximum so can fit in 16 bit memory space (short)
            currentSoundDbl = (32767*Math.sin(2*Math.PI*i*frequency/(double)SAMPLE_RATE));
            //We start a linear fading effect in the middle of the table.
            //STRATEGY: We calculate the current fading product between 1.0 and 0, descending a
            // certain step each iteration. Then we multiply it with the current sample.
            if(i>=(buffSize/4)){
                fader -=faderStep;//fader=fader-faderStep;
                currentSoundDbl= fader*currentSoundDbl;
            }

            //Sample compression to 16b representation.
            currentSound=(short)currentSoundDbl;
            //We put the LS Byte first, then we put the MS Byte of the current sound. As Big endian.
            //To achieve this, we use bit-hack operators.
            generatedSnd[j] = (byte)(currentSound & 0xFF);
            j++;
            generatedSnd[j] = (byte) ((currentSound & 0xFF00) >>> 8);
            j++;

        }
    }


    public void setMinimumSignal(double frequency){
        double maxValueDouble = SAMPLE_RATE/frequency;
        maxValueDouble = Math.floor(maxValueDouble);
        int MAX_VALUE = (int)maxValueDouble;
        generatedSnd = new byte[MAX_VALUE*bitDepth/8];
        short currentSound=0;
        int j = 0;//Auxiliar index

        for (int i = 0; i<(generatedSnd.length/2); i++) {
            currentSound = (short) (32767 * Math.sin(2 * Math.PI * i * frequency / (double) SAMPLE_RATE));

            //We put the LS Byte first, then we put the MS Byte of the current sound. As Big endian.
            //To achieve this, we use bit-hack operators.
            generatedSnd[j] = (byte)(currentSound & 0xFF);
            j++;
            generatedSnd[j] = (byte) ((currentSound & 0xFF00) >>> 8);
            j++;
        }
    }

    /*
     * This function will release the current AudioTrack instance, called audioHandler to do not
     * collapse available audio channels each time. Then it will store memory for a new AudioTrack
     * instance and play in a loop the audio sampled in generatedSnd[]
     * */
    public void playInfinite(){
        //We release the native memory behind the AudioTrack instance, as it cannot be managed by
        //Java VM's garbage collector.

        if (audioHandler != null)
            audioHandler.release();

        //New AudioTrack Instance
        audioHandler = new AudioTrack(
                AudioManager.STREAM_MUSIC,
                SAMPLE_RATE,
                AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                buffSize,
                AudioTrack.MODE_STATIC
        );

        //Write current sound to be played
        audioHandler.write(generatedSnd,0,generatedSnd.length);
        //We set up the infinite loop playing
        audioHandler.setLoopPoints(0,generatedSnd.length/2,-1);
        //Play sound
        audioHandler.play();
    }

    /*
     * This function will release the current AudioTrack instance, called audioHandler to do not
     * collapse available audio channels each time. Then it will store memory for a new AudioTrack
     * instance and play the audio sampled in generatedSnd[] till the end of samples.
     * */
    public void playLimited(){
        if (audioHandler != null)
            audioHandler.release();

        audioHandler = new AudioTrack(
                AudioManager.STREAM_MUSIC,
                SAMPLE_RATE,
                AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                buffSize,
                AudioTrack.MODE_STATIC
        );
        //Write current sound to be played
        audioHandler.write(generatedSnd,0,generatedSnd.length);
        //Play sound
        audioHandler.play();
    }

    /*
     * Stops audioTrack from playing state*/
    public void stop() {
        if (audioHandler != null)
            audioHandler.stop();
    }

    /*
     * Draws the generatedSnd[] samples to form a red wave, so we can appreciate the different
     * frequencies that we can play*/
    public void draw(Canvas canvas, int left, int top, int right, int bottom, int scale) {
        //Local variables set-up
        Path path = new Path(); // To store each point of the graphic
        Paint polyline = new Paint();// To paint the graphic following the path above
        polyline.setColor(Color.RED);// We set-up red color line
        polyline.setStrokeWidth(5.0f);// We set up the width of the line
        polyline.setStyle(Paint.Style.STROKE); //We set up the style, a continuous stroke

        int x = left;
        int valor = generatedSnd[0] + generatedSnd[1]*256;//First value

        int height = bottom-top;
        int y = valor*height/65536 + top;

        int width = right-left;
        int samples = (generatedSnd.length+1)/2/scale;//We cut the number of samples to be drawn

        boolean repeat = false;
        if (scale == 1){
            repeat = true;
        }

        int i = 0;
        path.moveTo(x,y);//Initial path point
        while(i<samples){
            x = x + width/samples;
            valor = generatedSnd[2*i] + generatedSnd[2*i+1]*256;
            y = valor*height/65536 + top;
            path.lineTo(x,y);
            canvas.drawPath(path,polyline);
            i++;
            //in organ mode we draw the note twice
            if (repeat && i == samples){
                i = 0;
                repeat = false;
            }
        }
    }
}