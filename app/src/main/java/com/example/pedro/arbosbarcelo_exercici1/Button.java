package com.example.pedro.arbosbarcelo_exercici1;

public class Button {

    private String text;
    private Rectangle area;

    public Button(String text, Rectangle area) {
        this.text = text;
        this.area = area;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Rectangle getArea() {
        return area;
    }

    public void setArea(Rectangle area) {
        this.area = area;
    }
}
