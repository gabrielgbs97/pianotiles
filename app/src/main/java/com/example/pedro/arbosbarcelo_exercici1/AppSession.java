package com.example.pedro.arbosbarcelo_exercici1;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.SurfaceHolder;

public class AppSession { //base parent class for menus and game modes
    protected Context context;
    protected int screenWidth;
    protected int screenHeight;
    protected Canvas canvas;
    protected MainView m;

    protected static int WIDTH;
    protected static int HEIGHT;

    // Dimensions of the ideal screen, to fit any other resolution
    protected int modelViewX = 1583;
    protected int modelViewY = 1080;
    protected int modelCenterX = modelViewX/2;
    protected int modelCenterY = modelViewY/2;

    // Drawing Ratio for this screen
    protected float ratioH = 1;
    protected float ratioV = 1;
    protected float ratio;
    
    //Some colors we will use
    protected static final int TEXT_COLOR = Color.argb(255,255,128,128);
    public static final int LIGHT_GRAY = 0xFFEEEEEE;


    protected Paint paint;

    public AppSession(Context context, MainView m, int screenWidth, int screenHeight)
    {
        this.context = context;

        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;

        this.m = m;

        // Ratio to fit the modelView resolution to this screen.
        // For fixed screen drawing or writing

        float ratio1 = modelViewX/(float) screenWidth;
        float ratio2 = modelViewY/(float) screenHeight;

        ratioH = 1/ratio1;
        ratioV = 1/ratio2;

    }

    public void update(long fps)
    {

    }

    //public void drawFunction()

    public void draw(SurfaceHolder holder)
    {

        if (holder.getSurface().isValid()) {
            //First we lock the area of memory we will be drawing to
            canvas = holder.lockCanvas();

            //draw a background color
            canvas.drawColor(Color.argb(255, 0, 0, 0));

            // Unlock and draw the scene
            paint = new Paint();

            holder.unlockCanvasAndPost(canvas);
        }
    }

    public AppSession handleInput(int x, int y){
        return this;
    }


    public void handleStopInput(int x, int y)
    {
    }

    protected int toScreenX(int in)
    {
        return (int)(in*ratioH);
    }

    protected int toScreenY(int in)
    {
        return (int)(in*ratioV);
    }

    protected int fromScreenX(int in)
    {
        return (int)(in/ratioH);
    }
    protected int fromScreenY(int in)
    {
        return (int)(in/ratioV);
    }

    // Draws text centered on the screen
    // Y relative to the proposed position (TOP, CENTER, BOTTOM)

    public void drawCenteredTextYTOP(Canvas canvas, String txt, int size, int color, int pos)
    {
        drawCenteredText(canvas, txt, size, color, pos);
    }

    public void drawCenteredTextYCENTER(Canvas canvas, String txt, int size, int color, int pos)
    {
        drawCenteredText(canvas, txt, size, color, pos+modelCenterY);
    }

    public void drawCenteredTextYBOTTOM(Canvas canvas, String txt, int size, int color, int pos)
    {
        drawCenteredText(canvas, txt, size, color, pos+modelViewY);
    }

    public void drawCenteredText(Canvas canvas, String txt, int size, int color, int pos)
    {
        Paint paintText = new Paint();
        paintText.setTextSize(toScreenX(size));

        // Horizontal centering
        paintText.setTextAlign(Paint.Align.CENTER);
        paintText.setColor(color);
        paintText.setAlpha(200);

        int xPos = modelViewX / 2;
        int yPos = pos;
        canvas.drawText(txt, toScreenX(xPos), toScreenY(yPos), paintText);
    }
}
