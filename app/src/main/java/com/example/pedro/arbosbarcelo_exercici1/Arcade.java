package com.example.pedro.arbosbarcelo_exercici1;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.view.SurfaceHolder;

public class Arcade extends Game {
    public Arcade(Context context, MainView m, int screenWidth, int screenHeight) {
        super(context, m, screenWidth, screenHeight);

        ref = HEIGHT*3; //start with only one row in the screen
        dref = 1; 
        ddref = screenHeight/118400.0; //set initial speed and increment
    }



    @Override
    public AppSession handleInput(int x, int y) {
        if (gameOver != null) {
            return handleGameOverInput(x,y); //in case of game over
        }
        Point p = new Point(x,y+ref);
        int idx = searchHit(p); //get tapped point
        if (idx < 0){
            return this; //If we tap something that is not a tile
        }
        if (!gameEnded) {
            handleTile(idx);
        }
        return this;
    }

    public void handleHit(int i){
        super.handleHit(i);
        points++; //increase score
    }

    public void handleMiss(int i){
        endGame();
    }

    @Override
    public void draw(SurfaceHolder holder) {
        checkSkippedTiles(); //check if a black tile passed through the screen without being tapped

        super.draw(holder);

        if (ref <= tiles[COLS*ROWS-1].getY()){ //if all tiles in the array have been passed
            generateInfiniteTiles(); //generates another array of tiles
        }

    }

    private void generateInfiniteTiles() {
        Rectangle[] oldTiles = tiles.clone();
        super.generateTiles(); //generate new tiles array
        for(int i =0;i<COLS*4;i++){ //mantain colors of tiles that are currently in the screen
            tiles[i].setColor(oldTiles[COLS*ROWS-(COLS*4-i)].getColor());
        }
        ref=0;
    }

    private void checkSkippedTiles() {
        int i = 0;
        while (tiles[i].getY() - ref - dref >= screenHeight){ //iterate over tiles that are under the screen
            if (tiles[i].getColor()==Color.BLACK){
                endGame(); //if one of them is still black, it's game over
            }
            i++;
        }
    }

    @Override public void drawPoints(){
        drawCenteredText(canvas, getScoreMessage(),150, TEXT_COLOR, modelCenterY*1/6);
    }



}
