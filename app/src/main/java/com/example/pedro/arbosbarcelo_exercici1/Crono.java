package com.example.pedro.arbosbarcelo_exercici1;

import android.content.Context;
import android.graphics.Point;
import android.view.SurfaceHolder;

public class Crono extends Game {

    private int currentMaxRef;
    private boolean[] hits = new boolean[ROWS+1]; //array that indicates if the black tile was tapped for every row
    private long initialTime = 0;
    private long stopTime;

    public Crono(Context context, MainView m, int screenWidth, int screenHeight) {
        super(context, m, screenWidth, screenHeight);

        for (int i=0;i<hits.length;i++){ //initialize array
            hits[i]=false;
        }

        ref = 0; //start with four rows in the screen
        ddref = 0;
        dref = screenHeight/32.0; //screen speed for when a black tile is tapped
    }

    @Override public void draw(SurfaceHolder holder){

        super.draw(holder);

        if(ref + screenHeight <= tiles[tiles.length-2].getY()){{ //if end is reached
            System.out.println("GAME OVER");
            endGame();
        }}
        //We limit next reference of the game to take into account. We do not let the game advance
        //through the next key to be hit.
        currentMaxRef=0;
        int i = 0;
        while(hits[i]){
            currentMaxRef-=HEIGHT;
            i++;
        }
        if (currentMaxRef >= ref){
            ref = currentMaxRef;
        }
    }

    @Override
    public AppSession handleInput(int x, int y){
        if (gameOver != null) {
            return handleGameOverInput(x,y); //in case of game over
        }
        Point p = new Point(x,y+ref);
        int i = searchHit(p); //get tapped point
        if (i < 0) {
            return this; //if no tile was tapped
        }
        handleTile(i);
        return this;
    }

    @Override
    public void handleHit(int i){
        super.handleHit(i);
        if (initialTime==0){ //set initial time (only once)
            initialTime = System.currentTimeMillis();
        }
        points++;
        //If hit all rows, we save the final time. It will be the score.
        if (points == super.ROWS){
            stopTime = System.currentTimeMillis() - initialTime;
            gameEnded=true;
        }
        hits[(int)(Math.floor(i/4))]=true; //set the row as hit
    }

    @Override
    protected void drawPoints(){
        drawCenteredText(canvas, "Remaining: "+(remainRows),150, TEXT_COLOR, modelCenterY*1/6);
        //Time print function
        long time;
        if (gameEnded){
            time = stopTime;
        } else if (initialTime == 0.0){
            time = 0;
        } else {
            time = System.currentTimeMillis()-initialTime;
        }

        drawCenteredText(canvas, "Score: "+writeTime(time), 150, TEXT_COLOR, modelCenterY * 2 / 6 + 5);
    }

    protected String writeTime(double time){ //write time in the desired format (MM:SS:CC)
        int cents = (int) (time/10.0)%100;

        int seconds = (int) Math.floor(time/1000.0)%60;
        int unitSeconds = seconds%10;
        int tenSeconds = (int) Math.floor(seconds/10);

        int mins = (int) Math.floor(time/60000.0);
        int unitMins = mins%10;
        int tenMins = (int) Math.floor(mins/10);

        return (tenMins+""+unitMins+":"+tenSeconds+""+unitSeconds+":"+cents);
    }

    @Override
    protected String getScoreMessage() {
        return "Time: "+writeTime(stopTime);
    }
}


