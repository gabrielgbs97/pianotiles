package com.example.pedro.arbosbarcelo_exercici1;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.SurfaceHolder;

public class GameOver extends Menu {
    String scoreMessage;

    public GameOver(Context context, MainView m, int screenWidth, int screenHeight, String scoreMessage) {
        super(context, m, screenWidth, screenHeight);
        this.scoreMessage = scoreMessage;
        this.menuOptions = "RETURN.".split("\\."); //only return button
        this.nButtons = this.menuOptions.length;
        this.FIRST_BUTTON_Y = (screenHeight * 3) / 4; //position of the button
        getButtons();
        this.title = "GAME OVER";
        this.scoreMessage = scoreMessage;
    }

    public void draw(SurfaceHolder holder) { //paint score string and return button
        if (holder.getSurface().isValid()) {
            this.canvas = holder.lockCanvas();
            this.canvas.drawColor(Color.argb(255, 255, 255, 255));
            this.paint = new Paint();
            drawButtons();
            drawScoreMessage();
            holder.unlockCanvasAndPost(this.canvas);
        }
    }

    private void drawScoreMessage() {
        drawCenteredText(this.canvas, this.scoreMessage, 114, TEXT_COLOR, fromScreenY((this.screenHeight * 10) / 30));
    }

    public AppSession handleInput(int x, int y) {
        if (this.buttons[0].getArea().contains(new Point(x, y))) { //if return button is tapped
            return new ChooseMenu(this.context, this.m, this.screenWidth, this.screenHeight);
        }
        return this;
    }
}
