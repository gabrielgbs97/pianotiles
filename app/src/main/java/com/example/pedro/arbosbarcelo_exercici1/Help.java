package com.example.pedro.arbosbarcelo_exercici1;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.SurfaceHolder;

public class Help extends Menu {
    public Help(Context context, MainView m, int screenWidth, int screenHeight) {
        super(context, m, screenWidth, screenHeight);
        this.menuOptions = "MAIN MENU.".split("\\."); //only main menu button
        this.nButtons = this.menuOptions.length;
        this.FIRST_BUTTON_Y = (screenHeight * 3) / 4; //position of the button
        getButtons();
        this.title = "HELP";
    }

    public void draw(SurfaceHolder holder) { //paints help explanation and button
        if (holder.getSurface().isValid()) {
            this.canvas = holder.lockCanvas();
            this.canvas.drawColor(Color.argb(255, 255, 255, 255));
            this.paint = new Paint();
            drawButtons();
            drawHelpInfo();
            holder.unlockCanvasAndPost(this.canvas);
        }
    }

    public void drawHelpInfo() { //text for game explanation
        drawCenteredText(this.canvas, "Tap the black tiles to score.", 114, TEXT_COLOR, fromScreenY((this.screenHeight * 10) / 30));
        drawCenteredText(this.canvas, "-Race: Score as many points", 114, TEXT_COLOR, fromScreenY((this.screenHeight * 12) / 30));
        drawCenteredText(this.canvas, "as possible out of 50 rows.", 114, TEXT_COLOR, fromScreenY((this.screenHeight * 13) / 30));
        drawCenteredText(this.canvas, "-Crono: Score 50 points in", 114, TEXT_COLOR, fromScreenY((this.screenHeight * 15) / 30));
        drawCenteredText(this.canvas, "the least amount of time.", 114, TEXT_COLOR, fromScreenY((this.screenHeight * 16) / 30));
        drawCenteredText(this.canvas, "-Arcade: Keep playing", 114, TEXT_COLOR, fromScreenY((this.screenHeight * 18) / 30));
        drawCenteredText(this.canvas, "until you miss.", 114, TEXT_COLOR, fromScreenY((this.screenHeight * 19) / 30));
    }

    public AppSession handleInput(int x, int y) {
        if (this.buttons[0].getArea().contains(new Point(x, y))) { //if button was tapped
            return new MainMenu(this.context, this.m, this.screenWidth, this.screenHeight);
        }
        return this;
    }
}
