package com.example.pedro.arbosbarcelo_exercici1;

import android.app.Activity;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.AudioFormat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;

import java.sql.SQLOutput;


public class MainActivity extends Activity {

    MainView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get a Display object to access screen details
        Display display = getWindowManager().getDefaultDisplay();
// Load the resolution of the screen into a Point object
        Point resolution = new Point();
        display.getSize(resolution);
// Create the instance of our MainView
        view = new MainView(this, resolution.x, resolution.y);
// And finally set the view for our game
        setContentView(view);
    }

//    @Override
//    protected void onTouch(){
//
//    }

    @Override
    protected void onPause() {
        super.onPause();
        view.pause();
    }

    // If the Activity is resumed make sure to resume our thread
    @Override
    protected void onResume() {
        super.onResume();
        view.resume();
    }


}
