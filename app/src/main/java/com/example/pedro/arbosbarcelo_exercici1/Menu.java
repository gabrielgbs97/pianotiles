package com.example.pedro.arbosbarcelo_exercici1;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.SurfaceHolder;

public class Menu extends AppSession { //base menu class
    public int BUTTON_OFFSET = screenHeight/6;
    public int FIRST_BUTTON_Y = screenHeight/5;
    public int BUTTON_SIZE = screenHeight/15;
    protected Button[] buttons;
    protected String[] menuOptions;
    protected int nButtons;
    String title;

    public Menu(Context context, MainView m, int screenWidth, int screenHeight) {
        super(context, m, screenWidth, screenHeight);
    }

    public void draw(SurfaceHolder holder) {

        if (holder.getSurface().isValid()) {
            //First we lock the area of memory we will be drawing to
            canvas = holder.lockCanvas();

            //draw a background color
            canvas.drawColor(Color.argb(255, 255, 255, 255));

            // Unlock and draw the scene
            paint = new Paint();
            drawButtons();

            holder.unlockCanvasAndPost(canvas);

        }
    }

    public void getButtons(){ //get buttons array
        buttons = new Button[nButtons];
        for (int i= 0; i<nButtons;i++) {
            Rectangle r = new Rectangle(0,FIRST_BUTTON_Y+BUTTON_OFFSET*i, //set next button position
                    screenWidth, BUTTON_SIZE, LIGHT_GRAY);
            buttons[i] = new Button(menuOptions[i],r); //get text for the button and create it
        }
    }

    public void drawButtons(){
        paint.setStrokeWidth(1.0f);
        int x;
        int y;

        for (int i=0; i<nButtons ; i++) {
            Button button = buttons[i];
            Rectangle r = button.getArea();

            paint.setColor(r.getColor());
            paint.setStyle(Paint.Style.FILL);
            canvas.drawRect(r.getX(),r.getY(),screenWidth,r.getY()+r.getHeight(),paint); //draw button rectangle
            drawCenteredText(canvas, button.getText(), (int)(r.getHeight()*3428/screenHeight), TEXT_COLOR,fromScreenY(r.getHeight() + r.getY())); //draw button text
        }

        //Draw title
        drawCenteredText(canvas, title,
                (int)(BUTTON_SIZE*3428/screenHeight),
                TEXT_COLOR,fromScreenY(screenHeight/8));

    }

    public int getHitButtonIndex(Point p){ //search tapped button
        for(int i = 0; i<nButtons; i++){
            if(buttons[i].getArea().contains(p))
                return i; //return index in array
        }
        return -1; //if no button was tapped return -1
    }

}
