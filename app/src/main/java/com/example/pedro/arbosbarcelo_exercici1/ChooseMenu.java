package com.example.pedro.arbosbarcelo_exercici1;

import android.content.Context;
import android.graphics.Point;


public class ChooseMenu extends Menu {

    public ChooseMenu(Context context, MainView m, int screenWidth, int screenHeight) {
        super(context, m, screenWidth, screenHeight);
        menuOptions = "RACE.CRONO.ARCADE.MAIN MENU".split("\\."); //String array for the options in the menu
        nButtons = menuOptions.length;
        getButtons();
        title = "CHOOSE MODE";
    }

    @Override
    public AppSession handleInput(int x, int y) {
        int idx = getHitButtonIndex(new Point(x,y)); //get index for the options array
        switch (idx){ //load the corresponding screen
            case 0:
                return new Race(context,m,screenWidth,screenHeight);
            case 1:
                return new Crono(context,m,screenWidth,screenHeight);
            case 2:
                return new Arcade(context,m,screenWidth,screenHeight);
            case 3:
                return new MainMenu(context,m,screenWidth,screenHeight);
        }
        return this; //if no button was tapped
    }
}