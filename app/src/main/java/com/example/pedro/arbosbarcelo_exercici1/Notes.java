package com.example.pedro.arbosbarcelo_exercici1;

public enum Notes {
  // 0     1    2     3    4     5   6     7     8      9     10   11
    DO1, DOS1, RE1, RES1, MI1, FA1, FAS1, SOL1, SOLS1, LA1, LAS1, SI1,
    DO2, DOS2, RE2, RES2, MI2, FA2, FAS2, SOL2, SOLS2, LA2, LAS2, SI2,//23
    DO3, DOS3, RE3, RES3, MI3, FA3, FAS3, SOL3, SOLS3, LA3, LAS3, SI3,//35
  // 36   37    38   39    40   41   42    43    44     45   46    47
    DO4, DOS4, RE4, RES4, MI4, FA4, FAS4, SOL4, SOLS4, LA4, LAS4, SI4,
  // 48   49    50   51    52   53   54    55    56     57   58    59
    DO5, DOS5, RE5, RES5, MI5, FA5, FAS5, SOL5, SOLS5, LA5, LAS5, SI5,//60
    DO6, DOS6, RE6, RES6, MI6, FA6, FAS6, SOL6, SOLS6, LA6, LAS6, SI6;

    private double LA4freq = 440;

    //get name of note from its ordinal
    public static String getName(int ord){
        String name = "";
        //first we find out the note
        switch(ord%12){
            case 0: name="C"; break;
            case 1: name="C#"; break;
            case 2: name="D"; break;
            case 3: name="D#"; break;
            case 4: name="E"; break;
            case 5: name="F"; break;
            case 6: name="F#"; break;
            case 7: name="G"; break;
            case 8: name="G#"; break;
            case 9: name="A"; break;
            case 10: name="A#"; break;
            case 11: name="B"; break;
        }
        //then we add the number corresponding to the eight
        int i = (int) (Math.floor(ord/12) + 1);
        return name + i;
    }


    //get object enum of note from its ordinal
    public static Notes getNote(int ord){
        String name = "";
        //first we find out the note
        switch(ord%12){
            case 0: name="DO"; break;
            case 1: name="DOS"; break;
            case 2: name="RE"; break;
            case 3: name="RES"; break;
            case 4: name="MI"; break;
            case 5: name="FA"; break;
            case 6: name="FAS"; break;
            case 7: name="SOL"; break;
            case 8: name="SOLS"; break;
            case 9: name="LA"; break;
            case 10: name="LAS"; break;
            case 11: name="SI"; break;
        }
        //then we add the eight and get the object
        int i = (int) (Math.floor(ord/12) + 1);
        return valueOf(name + i);
    }

    //get grequency of this note
    public double getFrequency(){
        int m = this.ordinal();
        int la4index = LA4.ordinal();
        return LA4freq*(Math.pow(Math.pow(2, (double) 1/12),m-la4index));
    }
}