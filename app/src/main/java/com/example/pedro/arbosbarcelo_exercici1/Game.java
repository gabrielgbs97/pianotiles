package com.example.pedro.arbosbarcelo_exercici1;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.SurfaceHolder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Random;

public class Game extends AppSession {


    protected Paint paint;

    protected final static int ROWS = 50;
    protected final static int COLS = 4;
    protected static int WIDTH;
    protected static int HEIGHT;
    protected Rectangle[] tiles; //list of tiles (Rectangle objects)

    int ref;    //referenece height to draw tiles
    double dref; //speed, next incrementation of reference
    double ddref;//acceleration

    //POINTS
    protected int points=0; //current player score
    protected int remainRows=ROWS; 
    protected boolean gameEnded;

    protected GameOver gameOver;
    protected NotePlayer music;
    protected double[] notesFreq;
    protected int nextNote =0;

    public Game(Context context, MainView m, int screenWidth, int screenHeight)
    {
        super(context, m, screenWidth, screenHeight);

        WIDTH = screenWidth/COLS;
        HEIGHT = screenHeight/4;

        tiles = new Rectangle[COLS*ROWS];
        generateTiles();
        gameEnded=false;
        gameOver = null;
        notesFreq = loadMusic();
    }

    public void update(long fps) {
    }

    public void draw(SurfaceHolder holder)
    {

        if (holder.getSurface().isValid()) {
            //First we lock the area of memory we will be drawing to
            if(gameOver!= null){
                gameOver.draw(holder);
                return;
            }

            canvas = holder.lockCanvas();

            //draw a background color
            canvas.drawColor(Color.argb(255, 255, 255, 255));

            // Unlock and draw the scene
            paint = new Paint();

            drawTiles();
            drawPoints();

            //Move forward keys
            dref+=ddref;
            ref-=dref;

            holder.unlockCanvasAndPost(canvas);
        }
    }

    public int searchHit(Point p){ //searches for the tile that was tapped
        for (int i=0; i<tiles.length;i++) {
            Rectangle r = tiles[i];
            if (r.contains(p)){
                return i;
            }
        }
        return -1; //if tapped point is not in a tile, return -1
    }

    public void handleTile(int i){ //given index in the tiles array, execute the corresponding code if it's tapped
        Rectangle r = tiles[i];
        if (r.isBlack()){ //if tile is black, it's a hit. Turn it gray
            r.setColor(LIGHT_GRAY);
            handleHit(i);
        } else { //if not, it's a miss. Turn it red
            r.setColor(Color.RED);
            handleMiss(i);
        }
    }

    protected String getScoreMessage(){
        return "Points: "+points;
    }

    //overriden in child classes
    public void handleHit(int i){ //play next note in the song
        music.setFullSignal(notesFreq[nextNote]);
        nextNote = (nextNote+1)%notesFreq.length;
        music.playLimited();
    }

    //overriden in child classes
    public void handleMiss(int i){

    }


    public void handleStopInput(int x, int y) {

    }

    protected AppSession handleGameOverInput(int x ,int y) { //handler for the game over menu
        gameOver.getButtons();
        if (gameOver.buttons[0].getArea().contains(new Point(x,y))){ //return to choose menu if the button is tapped
            return new ChooseMenu(context,m,screenWidth,screenHeight);
        }
        return this;
    }

    protected void endGame(){
        dref = 0;
        ddref = 0;
        long waitTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - waitTime < 1000) {} //wait one second
        gameEnded = true;
        gameOver = new GameOver(context, m, screenWidth, screenHeight, getScoreMessage()); //load game over menu
    }

    protected void generateTiles() {
        int x;
        int y=screenHeight-HEIGHT; // 3/4 screenHeight
        Random rand = new Random();
        int n;
        for (int i=0;i<ROWS; i++) {
            x = 0;
            n = rand.nextInt(COLS); //random index for the black tile
            for(int j=0; j<COLS; j++) {
                tiles[i*COLS+j] = new Rectangle(x,y,WIDTH,HEIGHT,Color.WHITE); //generate next tile
                if (n == j){
                    tiles[i*COLS+j].setColor(Color.BLACK); //make the corresponding tile black
                }
                x+=WIDTH; //go to the tile in the right
            }
            y-=HEIGHT; //go to the first tile in the above row
        }
    }

    protected void drawTiles() {
        paint.setStrokeWidth(1.0f);
        int x;
        int y;
        remainRows = ROWS; //reset remainRows

        for (int i=0; i< ROWS*COLS;i++){
            Rectangle r = tiles[i];
            x = r.getX();
            y = r.getY()-ref;
            //Performance enchantment
            //Evil conditional path
            if(screenHeight<=y) {//Case that  key is above the screen
                if (i%COLS==0){
                    remainRows--; //decrease remainRows until correct value
                }
                continue;
            }
            if (y<-HEIGHT)//Case that key is under the screen
                continue;

            paint.setColor(r.getColor()); //get tile color
            paint.setStyle(Paint.Style.FILL);

            canvas.drawRect(x,y,x+r.getWidth(),y+r.getHeight(),paint); //paint tile
            paint.setStyle(Paint.Style.STROKE);

            paint.setColor(Color.BLACK);
            canvas.drawRect(x,y,x+r.getWidth(),y+r.getHeight(),paint); //paint borders of the tile
        }
    }

    protected void drawPoints(){

    }

    private double[] loadMusic(){
        double[] freqs={1};
        music = new NotePlayer(true);
        String musicFileName = "Example";
        InputStream is = context.getResources().openRawResource(R.raw.example);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            is.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String jsonString = writer.toString();
        System.out.println("Readed string: \n"+jsonString);

        //We work with json representation of our music
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            if(jsonObject.has("notes")){
                System.out.println("HAY NOTAS");
            }

            System.out.println("Parsed json: \n"+jsonObject.toString());
            JSONArray notes = jsonObject.getJSONArray("notes");
            int nNotes =  notes.length();
            if (nNotes > 0){
                freqs = new double[nNotes];
                int currOrd = 0;
                double freq = 0;
                for (int i = 0; i<nNotes; i++) {
                    currOrd = notes.getInt(i);
                    freq = Notes.getNote(currOrd).getFrequency();
                    freqs[i]=freq;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            System.out.println("File not valid or malformed json");
        }

        return freqs;
    }

}


