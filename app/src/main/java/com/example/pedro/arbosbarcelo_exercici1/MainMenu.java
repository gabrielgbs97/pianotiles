package com.example.pedro.arbosbarcelo_exercici1;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;

import java.util.ArrayList;

public class MainMenu extends Menu {

    public MainMenu(Context context, MainView m, int screenWidth, int screenHeight) {
        super(context, m, screenWidth, screenHeight);
        menuOptions = "PLAY.HELP.EXIT".split("\\."); //String array for menu options
        nButtons = menuOptions.length;
        getButtons();
        title = "PIANO TILES";
    }

    @Override
    public AppSession handleInput(int x, int y) {
        int idx = getHitButtonIndex(new Point(x,y)); //get index in options array
        switch (idx){ //load corresponding screen
            case 0:
                return new ChooseMenu(context,m,screenWidth,screenHeight);
            case 1:
                return new Help(context,m,screenWidth,screenHeight);
            case 2:
                //Exit from app
                System.exit(0);
        }
        return this; //if no button was tapped
    }
}
