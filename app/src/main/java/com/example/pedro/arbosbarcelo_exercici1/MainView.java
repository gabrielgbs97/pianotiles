package com.example.pedro.arbosbarcelo_exercici1;

import android.view.SurfaceView;
import android.content.Context;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class MainView extends SurfaceView implements Runnable {
    private volatile boolean running;
    private Thread gameThread = null;

    // Game
    private AppSession game;

    // For drawing
    private SurfaceHolder ourHolder;

    // Control the fps
    long fps =120;

    MainView(Context context, int screenWidth, int screenHeight) {
        super(context);

        // Initialize our drawing objects
        ourHolder = getHolder();
        game = new MainMenu(context, this, screenWidth, screenHeight);
    }

    @Override

    public void run() {

        while (running) {
            long startFrameTime = System.currentTimeMillis();

            game.update(fps);
            game.draw(ourHolder);

            // Calculate the fps this frame
            long timeThisFrame = System.currentTimeMillis() - startFrameTime;
            if (timeThisFrame >= 1) {
                fps = 1000 / timeThisFrame;
            }
        }
    }

    // Clean up our thread if the game is stopped
    public void pause() {
        running = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            System.out.println("ERROR "+e.getMessage());
        }
    }

    // Make a new thread and start it
    // Execution moves to our run method
    public void resume() {
        gameThread = new Thread(this);
        gameThread.start();
        running = true;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        game = handleInput(motionEvent);
        return true;
    }

    public AppSession handleInput(MotionEvent motionEvent) {

        int x = (int) motionEvent.getX(0);
        int y = (int) motionEvent.getY(0);

        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:
                game = game.handleInput(x, y);
                break;

            case MotionEvent.ACTION_UP:
                game.handleStopInput(x, y);
                break;

        }

        return game;

    }
}
