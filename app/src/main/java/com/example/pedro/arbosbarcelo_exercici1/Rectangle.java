package com.example.pedro.arbosbarcelo_exercici1;

import android.graphics.Color;
import android.graphics.Point;

public class Rectangle {

    private int x;
    private int y;
    private int width;
    private int height;
    private int color;
    private boolean selected;


    public Rectangle(int x, int y, int width, int height, int color) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.color = color;
    }

    //returns true if given point is contained in this rectangle
    public boolean contains(Point p){
        boolean xIn = p.x >= x && p.x <= (x + width);
        boolean yIn = p.y >= y && p.y <= (y + height);
        return xIn && yIn;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isBlack(){
        return color == Color.BLACK;
    }


}