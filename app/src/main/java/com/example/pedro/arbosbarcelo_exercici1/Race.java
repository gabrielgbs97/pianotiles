package com.example.pedro.arbosbarcelo_exercici1;

import android.content.Context;
import android.graphics.Point;
import android.view.SurfaceHolder;

public class Race extends Game {


    public Race(Context context, MainView m, int screenWidth, int screenHeight) {
        super(context, m, screenWidth, screenHeight);

        ref = super.HEIGHT*3; //start with one row in the screen
        dref = 1;
        ddref = screenHeight/59200.0; //set initial speed and its icrease
    }

    @Override
    public void draw(SurfaceHolder holder) {
        if(ref + screenHeight + dref < tiles[tiles.length-1].getY()){{ //if end is reached
            endGame();
        }}
        super.draw(holder);
    }

    public AppSession handleInput(int x, int y) {
        if (gameOver != null) {
            return handleGameOverInput(x,y); //in case of game over
        }
        Point p = new Point(x,y+ref);
        int idx = searchHit(p); //get tapped point
        if (idx < 0){
            return this; //If we tap something that is not a tile
        }
        handleTile(idx);
        return this;
    }

    public void handleHit(int i){
        super.handleHit(i);
        points++; //increase score if hit
    }

     @Override public void drawPoints(){
         drawCenteredText(canvas, "Points: "+points, 150, TEXT_COLOR, modelCenterY*2-modelCenterY*1/6);
         drawCenteredText(canvas, "Remaining: "+remainRows,150, TEXT_COLOR, modelCenterY*1/6);
     }

}
